Englický dikcionář pro kontemporární žurnalismus
================================================

| English | Czechish |
|------------|----------------|
| bivalve | škeble |
| carbohydrates | karbohydráty |
| crucial problem | kruciální problém |
| crystal clear | krystalově čistý |
| descending order | descendenční pořadí |
| foreign body | cizí tělo |
| mammal-like reptiles | savcům podobní plazi |
| polar bear | polární medvěd |
| police officer | policejní důstojník |
| preemptive strike | preemptivní úder |
| shipworm | lodní červ |