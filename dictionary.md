Englický dikcionář pro kontemporární žurnalismus
================================================

| English | Czechish | Česky | Poznámka |
|------------|----------------|--|--|
| bivalve | škeble | mlž | viz shipworm |
| bilaterally symmetric | osově souměrný | dvojstranně symetrický | živočich |
| carbohydrates | karbohydráty | sacharidy ||
| crucial problem | kruciální problém | zásadní problém ||
| crystal clear | krystalově čistý | křišťálově čistý ||
| ctenophore | ctenofor(a) | žebernatka ||
| Department of Agriculture | oddělení pro agrikulturu | Ministerstvo zemědělství ||
| descending order | descendenční pořadí | sestupné pořadí / sestupně ||
| fava bean | fazolový bob | bob obecný | Pompeje, rok 79 |
| foreign body | cizí tělo | cizí těleso ||
| mammal-like reptiles | savcům podobní plazi | savcovití plazi ||
| offensive painting | ofenzivní malba | urážlivá malba ||
| polar bear | polární medvěd | lední medvěd ||
| police officer | policejní důstojník | policista ||
| preemptive strike | preemptivní úder | preventivní úder ||
| a ring for a sword belt | prsten z popruhu k meči | kroužek z opasku s mečem ||
| shipworm | lodní červ | sášeň lodní ||
| sulfur spring | sulfurové jezírko | sirný pramen ||
